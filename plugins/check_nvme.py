#!/usr/bin/env python3

import json
import sys
from subprocess import check_output, STDOUT, CalledProcessError


def main():
    disks_output = check_output([
        '/usr/sbin/nvme',
        'list',
        '--output-format=json',
    ])
    disks_output = json.loads(disks_output.decode())

    devices = [d['DevicePath'] for d in disks_output['Devices']]
    if len(devices) == 0:
        print('UNKNOWN - No nvme devices were detected')
        sys.exit(ExitCodes.Unknown)

    exit_code = 0
    exit_messages = []

    for device in devices:
        code, messages = check_device(device)
        exit_messages += messages

        # If we ever add a unknown exit code, we need to rework this function
        if code > exit_code:
            exit_code = code

    print(format_nagios_message(exit_code, exit_messages))
    sys.exit(exit_code)


def check_device(device):
    output = check_output([
        '/usr/sbin/nvme',
        'smart-log',
        device,
        '--output-format=json'
    ])

    data = json.loads(output.decode())

    code = 0
    messages = []
    if data['critical_warning'] != 0:
        code = 2
        messages.append(f'{device} has critical warning')

    if data['media_errors'] != 0:
        code = 2
        messages.append(f'{device} has media errors')

    if data['num_err_log_entries'] != 0:
        code = 2
        messages.append(f'{device} has errors logged')

    if data['percent_used'] >= 80:
        code = 1
        messages.append(f'{device} has reached {data["percent_used"]} percentage used')

    if data['avail_spare'] <= data['spare_thresh'] + 5:
        code = 1
        messages.append(f'{device} available spare is smaller than threshold+5%')

    if data['avail_spare'] <= data['spare_thresh']:
        code = 2
        messages.append(f'{device} available spare is smaller than threshold')

    if code == 0:
        messages.append(f'{device} has no issues')

    return code, messages


def format_nagios_message(code, messages):
    if code == ExitCodes.OK:
        messages.insert(0, 'OK - No issues detected')
    elif code == ExitCodes.Warning:
        messages.insert(0, 'WARNING - Issues were detected')
    elif code == ExitCodes.Critical:
        messages.insert(0, 'CRITICAL - Issues were detected')
    else:
        messages.insert(0, 'UNKNOWN - Issues were detected')

    message = '\n'.join(messages)

    return message


class ExitCodes:
    OK = 0
    Warning = 1
    Critical = 2
    Unknown = 3


if __name__ == '__main__':
    main()

