#!/usr/bin/env python3
import sys
from argparse import ArgumentParser
from subprocess import check_output


def parse_args():
    parser = ArgumentParser()

    parser.add_argument(
        '--warning_count',
        help='Warning threshold for queue count',
        type=int, default=5
    )
    parser.add_argument(
        '--critical_count',
        help='Critical threshold for queue count',
        type=int, default=10
    )
    parser.add_argument(
        '--warning_size',
        help='Warning threshold for queue size in KB',
        type=int, default=10000
    )
    parser.add_argument(
        '--critical_size',
        help='Critical threshold for queue size in KB',
        type=int, default=20000
    )

    args = parser.parse_args()

    if args.critical_count < args.warning_count:
        parser.error('critical_count must be higher than warning_count')

    if args.critical_size < args.warning_size:
        parser.error('critical_size must be higher than warning_size')

    return args


def main():
    args = parse_args()
    output = check_output(['/usr/bin/mailq'])
    last_line = output.splitlines()[-1].decode()
    if last_line == 'Mail queue is empty':
        print(f'OK - {last_line}')
        sys.exit(0)

    stats = last_line.split()
    queue_size = int(stats[1])
    queue_count = int(stats[4])

    if queue_count > args.critical_count:
        print(f'CRITICAL - Queue has {queue_count} messages (>{args.critical_count})')
        sys.exit(2)
    if queue_count > args.warning_count:
        print(f'WARNING - Queue has {queue_count} messages (>{args.warning_count})')
        sys.exit(1)
    if queue_size > args.critical_size:
        print(f'CRITICAL - Queue size is {queue_size}KB (>{args.critical_size}KB)')
        sys.exit(2)
    if queue_size > args.warning_size:
        print(f'WARNING - Queue size is {queue_size}KB (>{args.warning_size}KB)')
        sys.exit(1)

    print(f'OK - {queue_count} messages in queue ({queue_size}KB)')
    sys.exit(0)


if __name__ == '__main__':
    main()
