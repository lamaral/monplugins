#!/usr/bin/env python3
"""
This is a Nagios script which queries RIPE Stats, retrieves information
about a subnet from the looking glass and checks if the ASPATH for the
subnet matches what is expected

Below is the JSONPath query that yelds all of the ASPaths
$.data.rrcs[*].peers[*].as_path
"""

import json
import urllib.request
import pygraphviz as pgv


def calculate_thickness(adjacencies, min, max):
    return ((adjacencies - min) / (max - min)) * (4 - 1) + 1

MIN_ADJACENCIES = 3
url = 'https://stat.ripe.net/data/looking-glass/data.json?resource={}'
# resource = '2a0e:b107:532::/48'
# resource = '44.174.203.0/24'
# resource = '31.210.148.0/24'
resource = '2602:ff52::/48'
with urllib.request.urlopen(url.format(resource)) as response:
    # TODO: Check if the request was actually successful
    data = json.loads(response.read())

    # TODO: Check if json data has status ok and status_code 200

as_paths = []
for rrc in data['data']['rrcs']:
    for peer in rrc['peers']:
        as_paths.append(peer['as_path'].split(' '))

# Here we reverse the order of the as_path entries
as_paths = [li[::-1] for li in as_paths]
# print(as_paths)

A = pgv.AGraph()
A.graph_attr["splines"] = 'False'  # ortho, polyline, curved, spline, none
A.node_attr['shape'] = 'ellipse'
A.graph_attr["dpi"] = "70"  # str(int(settings.GRAPH_DPI)/3*2)
A.graph_attr["margin"] = '0.5'
A.graph_attr["rankdir"] = 'LR'
A.graph_attr["ranksep"] = '0.5'
A.graph_attr["nodesep"] = '0.1'

asn_adjacencies = {}

for path in as_paths:

    for _ in range(0, len(path) - 1):
        if (path[_], path[_ + 1]) in asn_adjacencies:
            asn_adjacencies[(path[_], path[_ + 1])] += 1
        else:
            asn_adjacencies[(path[_], path[_ + 1])] = 1

good_adjacencies = {
    k: v for k, v in asn_adjacencies.items()
    if v >= MIN_ADJACENCIES and k[0] != k[1]
}

# max_adj = max(good_adjacencies.values())
max_adj = 20
min_adj = min(good_adjacencies.values())

for adj, weight in good_adjacencies.items():
    if weight > 20:
        thickness = 20
    else:
        thickness = weight
    A.add_edge(
        adj[0], adj[1],
        penwidth=calculate_thickness(thickness, min_adj, max_adj),
        label=weight
    )

A.layout()
A.draw('test.svg', prog='dot')
