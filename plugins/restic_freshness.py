import json
import platform
import sys
from datetime import datetime, timedelta
from dateutil.parser import isoparse
from subprocess import check_output

# Make sure to keep --host as the last argument
RESTIC_COMMAND = [
    '/usr/bin/restic', 'snapshots',
    '--json', '--last', '--tag', 'automated', '--host'
]
TOO_OLD_HOURS = 26


def main():
    snapshots = get_snapshots()
    total_snapshots = len(snapshots)

    if total_snapshots == 0:
        print('CRITICAL: No snapshots were found for this host')
        sys.exit(2)

    annotate_snapshots(snapshots)

    total_too_old = len(list(ss for ss in snapshots if ss['too_old']))

    if total_too_old == total_snapshots:
        print(
            f'CRITICAL: There was no backup in the last {TOO_OLD_HOURS} hours'
        )
        sys.exit(2)
    elif total_too_old > 0:
        print(f'WARNING: There are backups older than {TOO_OLD_HOURS} hours')
        sys.exit(1)

    if total_snapshots == 1:
        age = snapshots[0]["age"].seconds/3600
        print(f'OK: Oldest backup is {age:.2f} hours old')
    else:
        print(f'OK: Last backups are newer than {TOO_OLD_HOURS} hours')

    sys.exit(0)

def get_snapshots():
    # Append the hostname to the restic command
    command = RESTIC_COMMAND + [platform.node()]
    last_snapshots = json.loads(check_output(command).decode('utf-8'))
    return last_snapshots


def annotate_snapshots(snapshots):
    for snapshot in snapshots:
        timestamp = isoparse(snapshot['time'])
        age = datetime.now(timestamp.tzinfo) - timestamp

        snapshot['age'] = age
        snapshot['too_old'] = (age > timedelta(hours=TOO_OLD_HOURS))

        # We don't want to bother with persistent snapshots
        if 'persist' in snapshot['tags']:
            snapshot['too_old'] = False


if __name__ == '__main__':
    main()
