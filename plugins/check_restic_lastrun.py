#!/usr/bin/env python3

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import time
from argparse import ArgumentParser
from datetime import datetime


DEFAULT_WARNING_THRESHOLD = 8000
DEFAULT_CRITICAL_THRESHOLD = 0


def get_args():
    parser = ArgumentParser(
        description=(
            'Check if the time of the last restic backup is '
            'longer ago than -w (warning) or -c (critical)'
        )
    )
    parser.add_argument(
        '-w',
        dest='warning',
        type=int,
        default=DEFAULT_WARNING_THRESHOLD,
        required=False,
        help=(
            'Warning threshold in seconds. '
            f'Default is {DEFAULT_WARNING_THRESHOLD}. '
            '0 = disable warning.'
        )
    )
    parser.add_argument(
        '-c',
        dest='critical',
        type=int,
        default=DEFAULT_CRITICAL_THRESHOLD,
        required=False,
        help=(
            'Critical threshold in seconds. '
            f'Default is {DEFAULT_CRITICAL_THRESHOLD}. '
            '0 = disable critical.'
        )
    )
    parser.add_argument(
        '-v', dest='verbose', action='store_true', required=False,
        help='Enable verbose output'
    )

    args = parser.parse_args()

    if args.critical > 0 and args.warning >= args.critical:
        err = (
            f'Warning threshold ({args.warning}) higher or equals '
            f'critical threshold ({args.critical})'
        )
        raise CheckException(ExitCodes.warning, err)

    return args


def main():
    try:
        args = get_args()
        restic_state_file = '/run/restic_lastrun'

        code, reason = check(args, restic_state_file)
    except CheckException as e:
        code, reason = e.args[0], e.args[1]

    print_nagios_message(code, reason)
    exit(code)


def check(args, restic_state_file):
    print_debug(args, 'Open file {restic_state_file}')
    try:
        with open(restic_state_file, 'r') as fd:
            content = fd.read()
            last_run = int(content.replace("\n", ''))
    except IOError:
        err = f'cannot read statefile {restic_state_file}'
        return ExitCodes.critical, err
    except ValueError:
        err = f'statefile {restic_state_file} is corrupt'
        return ExitCodes.critical, err

    now = int(time.time())
    age_sec = now - last_run
    age_hour = age_sec/3600

    print_overview(args, last_run, now, age_sec)

    last_run_str = datetime.utcfromtimestamp(last_run)
    output_sec = f'Last run was at {last_run_str} ({age_hour:.0f} hours ago)'

    if args.critical > 0 and age_sec >= args.critical:
        return ExitCodes.critical, output_sec
    if args.warning > 0 and age_sec >= args.warning:
        return ExitCodes.warning, output_sec

    return ExitCodes.ok, output_sec


def print_nagios_message(code, reason):
    if code == ExitCodes.ok:
        state_text = 'OK'
    elif code == ExitCodes.warning:
        state_text = 'WARNING'
    elif code == ExitCodes.critical:
        state_text = 'CRITICAL'
    else:
        state_text = 'UNKNOWN'
    print(f'{state_text} - {reason}')


def print_debug(args, text):
    if args.verbose:
        print(text)


def print_overview(args, last_run, now, age_sec):
    d_last_r = datetime.utcfromtimestamp(last_run)
    d_now = datetime.utcfromtimestamp(now)
    print_debug(args, f'Last run: {last_run} ({d_last_r} UTC)')
    print_debug(args, f'Now:      {now} ({d_now} UTC)')
    print_debug(args, f'Diff:     {age_sec}')
    print_debug(args, f'Warning:  {args.warning}')
    print_debug(args, f'Critical: {args.critical}')
    print_debug(args, '')


class ExitCodes:
    ok = 0
    warning = 1
    critical = 2
    unknown = 3


class CheckException(Exception):
    """Raised to exit with a valid nagios state"""
    pass


if __name__ == '__main__':
    main()
