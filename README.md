# monplugins

This repository contains a set of monitoring plugins used on my servers. 

To add a new plugin, just add the file to the `plugins` folder and it will be installed automatically. 